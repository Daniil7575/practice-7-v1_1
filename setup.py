from setuptools import setup, find_packages


__version__ = '1.0.0'

setup(
    name='Task1',
    version=__version__,
    packages=find_packages(),
    python_requires='>=3.7'
)
