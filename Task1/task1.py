import random as rnd
from string import ascii_letters
import sys


DICTIONARY = ascii_letters + '1234567890!@#$%^&*()_+=-{}[]'

def gen_password(length: int) -> str:
    if length < 8:
        raise ValueError('Password is too short')
    if length > 32:
        raise ValueError('Password is too long')

    return ''.join(DICTIONARY[rnd.randint(0, len(DICTIONARY) - 1)] for _ in range(length))


def main():
    password = gen_password(int(sys.argv[1]))
    with open('target/result.txt', 'w+') as file:
        file.write(password)


if __name__ == '__main__':
    main()
